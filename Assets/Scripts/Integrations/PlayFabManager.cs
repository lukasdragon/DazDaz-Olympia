﻿using olympia.Data;
using Photon.Pun;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;

namespace olympia.Integrations
{
    public sealed class PlayFabManager : MonoBehaviour
    {
        public string UserID { get; private set; }
        public string PhotonToken { get; private set; }

        public bool IsAuthenticated { get; private set; }

        public event SimpleEventHandler AuthenticatedEvent;

        public event SimpleEventHandler<PlayFabError> FailedToAuthenticateEvent;

        private void Start()
        {
            Debug.Log("Logging into PlayFab.");

            var id = PlayerPrefs.GetString("UserID", System.Guid.NewGuid().ToString());

            if (!PlayerPrefs.HasKey("UserID"))
            {
                PlayerPrefs.SetString("UserID", id);
                PlayerPrefs.Save();
            }

            Debug.Log($"logging in / registering with id {id}");

            //We make a request to log-in with PlayFab. If this is successful, we request a token to authenticate with PUN2.
            var request = new LoginWithCustomIDRequest {CustomId = id, CreateAccount = true};
            PlayFabClientAPI.LoginWithCustomID(request, RequestPhotonToken, OnLoginFailure);
        }


        private void RequestPhotonToken(LoginResult loginResult)
        {
            UserID = loginResult.PlayFabId;

            Debug.Log("PlayFab authenticated, requesting photon token...");

            PlayFabClientAPI.GetPhotonAuthenticationToken(new GetPhotonAuthenticationTokenRequest()
            {
                PhotonApplicationId = PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime
            }, OnAuthenticated, OnLoginFailure);
        }


        private void OnLoginFailure(PlayFabError error)
        {
            Debug.Log($"Failed to login... {error.Error}");
            OnFailedToAuthenticateEvent(error);
        }


        private void OnAuthenticated(GetPhotonAuthenticationTokenResult getPhotonAuthenticationTokenResult)
        {
            PhotonToken = getPhotonAuthenticationTokenResult.PhotonCustomAuthenticationToken;

            AuthenticatedEvent?.Invoke();
            IsAuthenticated = true;
        }

        private void OnFailedToAuthenticateEvent(PlayFabError data)
        {
            FailedToAuthenticateEvent?.Invoke(data);
        }
    }
}