using olympia.Integrations;
using Zenject;

namespace olympia.Installers
{
    public class PlayFabInstaller : MonoInstaller<PlayFabInstaller>
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<PlayFabManager>().FromNewComponentOnRoot().AsSingle().NonLazy();
        }
    }
}