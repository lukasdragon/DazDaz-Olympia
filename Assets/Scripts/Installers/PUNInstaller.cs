using olympia.Networking;
using UnityEngine;
using Zenject;

namespace olympia.Installers
{
    [CreateAssetMenu(fileName = "PUNInstaller", menuName = "Installers/PUNInstaller")]
    public class PUNInstaller : ScriptableObjectInstaller<PUNInstaller>
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<NetworkLauncher>().AsSingle().NonLazy();
        }
    }
}