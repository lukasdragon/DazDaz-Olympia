using olympia.Data;
using olympia.Gameplay.Chess;
using UnityEngine;
using Zenject;

namespace olympia.Installers
{
    public class ChessGameInstaller : MonoInstaller<ChessGameInstaller>
    {
        [SerializeField] private ChessBoardConfiguration _data;


        public override void InstallBindings()
        {
            var instance = new PunChessDataManager(_data);
            instance.Initialize();
            Container.BindInterfacesAndSelfTo<PunChessDataManager>().FromInstance(instance).AsSingle().NonLazy();


            Container.BindInterfacesAndSelfTo<ChessMatchManager>().FromNewComponentOnRoot().AsSingle();
        }
    }
}