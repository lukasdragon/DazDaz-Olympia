﻿using System.Collections.Generic;
using olympia.Data;

namespace olympia.Interfaces
{
    public interface IMatchManager
    {
        /// <summary>
        /// Called when the match starts.
        /// </summary>
        event SimpleEventHandler MatchStart;
        
        /// <summary>
        /// Called when the match ends.
        /// </summary>
        event SimpleEventHandler<EndGameEventType> MatchEnd;
        
        /// <summary>
        /// Called at the start of any players turn.
        /// </summary>
        event SimpleEventHandler TurnStart;
        
        /// <summary>
        /// Called when a move is received over the network.
        /// </summary>
        event SimpleEventHandler<ChessMove> MoveReceived;
        
        /// <summary>
        /// Called when the state of the game needs to be cleaned up and made ready for another match.
        /// </summary>
        event SimpleEventHandler Cleanup;

        List<ChessMove> MoveList { get; }

        /// <summary>
        /// Whether or not the match has started.
        /// </summary>
        bool HasStarted { get; }

        /// <summary>
        /// The number of turns this match, including this turn.
        /// </summary>
        int TurnCount { get; }

        /// <summary>
        /// The color of the local player.
        /// </summary>
        PieceColorEnum LocalPlayerColor { get; }

        /// <summary>
        /// Whether or not it is currently the turn of the local player.
        /// </summary>
        bool IsLocalPlayerTurn { get; }

        /// <summary>
        /// Submit a move to the server.
        /// </summary>
        /// <param name="move"></param>
        void SubmitMove(ChessMove move);

        /// <summary>
        /// Declares to the server that the match is over.
        /// </summary>
        /// <param name="reason">The reason that the match is over.</param>
        void EndGame(EndGameEventType reason);
    }
}