using System.Collections.Generic;
using UnityEngine;

namespace olympia.Gameplay.Chess.Pieces
{
    public class RookPiece : ChessPiece
    {
        public override Vector2Int[] GetLegalMoves(Vector2Int boardSize)
        {
            var legalMoves = new List<Vector2Int>();

            ChessPiece piece = null;


            //Forward
            for (var y = 1; y < boardSize.y - Position.y; y++)
            {
                var pos = new Vector2Int(Position.x, Position.y + y);
                if (PieceManager.GetPieceAtPosition(pos, out piece))
                {
                    if (piece.Color != Color) legalMoves.Add(pos);

                    break;
                }

                legalMoves.Add(pos);

                piece = null;
            }

            //Backwards
            for (var y = 1; y < Position.y + 1; y++)
            {
                var pos = new Vector2Int(Position.x, Position.y - y);
                if (PieceManager.GetPieceAtPosition(pos, out piece))
                {
                    if (piece.Color != Color) legalMoves.Add(pos);

                    break;
                }

                legalMoves.Add(pos);

                piece = null;
            }


            //Right
            for (var x = 1; x < boardSize.x - Position.x; x++)
            {
                var pos = new Vector2Int(Position.x + x, Position.y);
                if (PieceManager.GetPieceAtPosition(pos, out piece))
                {
                    if (piece.Color != Color) legalMoves.Add(pos);

                    break;
                }

                legalMoves.Add(pos);

                piece = null;
            }


            //Left
            for (var x = 1; x < Position.x + 1; x++)
            {
                var pos = new Vector2Int(Position.x - x, Position.y);

                if (PieceManager.GetPieceAtPosition(pos, out piece))
                {
                    if (piece.Color != Color) legalMoves.Add(pos);

                    break;
                }

                legalMoves.Add(pos);

                piece = null;
            }

            return legalMoves.ToArray();
        }
    }
}