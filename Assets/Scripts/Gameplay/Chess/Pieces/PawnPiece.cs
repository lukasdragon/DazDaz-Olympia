using System.Collections.Generic;
using olympia.Data;
using UnityEngine;

namespace olympia.Gameplay.Chess.Pieces
{
    public class PawnPiece : ChessPiece
    {
        public override Vector2Int[] GetLegalMoves(Vector2Int boardSize)
        {
            var legalMoves = new List<Vector2Int>();

            var direction = Color == PieceColorEnum.Black ? -1 : 1;

            ChessPiece piece = null;

            //Basic step forward if there isn't a piece in the way.
            var forwardOne = new Vector2Int(Position.x, Position.y + 1 * direction);

            if (!PieceManager.GetPieceAtPosition(forwardOne, out piece) && forwardOne.y >= 0 &&
                forwardOne.y < boardSize.y)
            {
                legalMoves.Add(forwardOne);
                piece = null;
            }


            //Capture one piece forward to the left
            var leftCapture = new Vector2Int(Position.x - 1, Position.y + 1 * direction);
            if (PieceManager.GetPieceAtPosition(leftCapture, out piece))
            {
                if (piece.Color != Color) legalMoves.Add(leftCapture);

                piece = null;
            }

            //Capture one piece forward to the right
            var rightCapture = new Vector2Int(Position.x + 1, Position.y + 1 * direction);
            if (PieceManager.GetPieceAtPosition(rightCapture, out piece))
            {
                if (piece.Color != Color) legalMoves.Add(rightCapture);

                piece = null;
            }


            //Two steps forward for the first move
            if (!HasMoved)
            {
                var forwardTwo = new Vector2Int(Position.x, Position.y + 2 * direction);

                if (!PieceManager.GetPieceAtPosition(forwardTwo, out piece))
                {
                    legalMoves.Add(forwardTwo);
                    piece = null;
                }
            }


            return legalMoves.ToArray();
        }
    }
}