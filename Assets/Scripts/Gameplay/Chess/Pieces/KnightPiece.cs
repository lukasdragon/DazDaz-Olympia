using System.Collections.Generic;
using UnityEngine;

namespace olympia.Gameplay.Chess.Pieces
{
    public class KnightPiece : ChessPiece
    {
        public override Vector2Int[] GetLegalMoves(Vector2Int boardSize)
        {
            var legalMoves = new List<Vector2Int>
            {
                new Vector2Int(Position.x + 1, Position.y + 2),
                new Vector2Int(Position.x + 2, Position.y + 1),
                new Vector2Int(Position.x + 2, Position.y - 1),
                new Vector2Int(Position.x + 1, Position.y - 2),
                new Vector2Int(Position.x - 1, Position.y + 2),
                new Vector2Int(Position.x - 2, Position.y + 1),
                new Vector2Int(Position.x - 2, Position.y - 1),
                new Vector2Int(Position.x - 1, Position.y - 2)
            };

            var movesToRemove = new List<Vector2Int>(8);

            foreach (var movePosition in legalMoves)
            {
                if (movePosition.x < 0 || movePosition.x >= boardSize.x)
                {
                    movesToRemove.Add(movePosition);
                    continue;
                }

                if (movePosition.y < 0 || movePosition.y >= boardSize.y)
                {
                    movesToRemove.Add(movePosition);
                    continue;
                }

                if (!PieceManager.GetPieceAtPosition(movePosition, out var piece)) continue;
                if (piece.Color == Color) movesToRemove.Add(movePosition);
            }

            foreach (var move in movesToRemove) legalMoves.Remove(move);


            return legalMoves.ToArray();
        }
    }
}