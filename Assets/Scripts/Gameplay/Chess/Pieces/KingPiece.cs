using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace olympia.Gameplay.Chess.Pieces
{
    public class KingPiece : ChessPiece
    {
        public override Vector2Int[] GetLegalMoves(Vector2Int boardSize)
        {
            var legalMoves = new List<Vector2Int>();

            for (var x = -1; x <= 1; x++)
            for (var y = -1; y <= 1; y++)
            {
                //If this is the center, skip
                if (x == 0 && y == 0) continue;

                var pos = new Vector2Int(Position.x + x, Position.y + y);

                if (pos.x >= boardSize.x || pos.x < 0) continue;

                if (pos.y >= boardSize.y || pos.y < 0) continue;

                if (!PieceManager.IsPieceAtPosition(pos)) legalMoves.Add(pos);
            }

            return legalMoves.ToArray();
        }

        public override void OnCaptured(uint capturerID)
        {
            PieceManager.KingCaptured();
            base.OnCaptured(capturerID);
        }
    }
}