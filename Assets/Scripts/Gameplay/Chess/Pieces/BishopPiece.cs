using System.Collections.Generic;
using UnityEngine;

namespace olympia.Gameplay.Chess.Pieces
{
    public class BishopPiece : ChessPiece
    {
        public override Vector2Int[] GetLegalMoves(Vector2Int boardSize)
        {
            var legalMoves = new List<Vector2Int>();

            ChessPiece piece = null;

            //Forward right
            for (var i = 1; i < boardSize.y - Position.y; i++)
            {
                var pos = new Vector2Int(Position.x + i, Position.y + i);


                if (pos.x >= boardSize.x) break;


                if (PieceManager.GetPieceAtPosition(pos, out piece))
                {
                    if (piece.Color != Color) legalMoves.Add(pos);

                    break;
                }

                legalMoves.Add(pos);
            }

            //Forward Left
            for (var i = 1; i < boardSize.y - Position.y; i++)
            {
                var pos = new Vector2Int(Position.x - i, Position.y + i);

                if (pos.x < 0) break;

                if (PieceManager.GetPieceAtPosition(pos, out piece))
                {
                    if (piece.Color != Color) legalMoves.Add(pos);

                    break;
                }

                legalMoves.Add(pos);
            }


            //Backwards Right
            for (var i = 1; i < Position.y + 1; i++)
            {
                var pos = new Vector2Int(Position.x + i, Position.y - i);

                if (pos.x >= boardSize.x) break;

                if (PieceManager.GetPieceAtPosition(pos, out piece))
                {
                    if (piece.Color != Color) legalMoves.Add(pos);

                    break;
                }

                legalMoves.Add(pos);
            }


            //Backwards Left
            for (var i = 1; i < Position.y + 1; i++)
            {
                var pos = new Vector2Int(Position.x - i, Position.y - i);

                if (pos.x < 0) break;

                if (PieceManager.GetPieceAtPosition(pos, out piece))
                {
                    if (piece.Color != Color) legalMoves.Add(pos);

                    break;
                }

                legalMoves.Add(pos);
            }


            return legalMoves.ToArray();
        }
    }
}