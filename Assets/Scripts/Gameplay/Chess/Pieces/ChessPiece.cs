using System.Collections.Generic;
using MoreMountains.Feedbacks;
using olympia.Data;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace olympia.Gameplay.Chess.Pieces
{
    /// <summary>
    ///     Monobehaviour that implements basic functionality that all pieces require.
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer), typeof(Collider2D))]
    public abstract class ChessPiece : SerializedMonoBehaviour
    {
        //Settings for the editor
        [Required] [SerializeField] private Sprite _blackSprite;

        [Required] [SerializeField] private Sprite _whiteSprite;

        [ReadOnly] [HideInEditorMode] [SerializeField]
        protected bool IsAlive = true;

        [ReadOnly] [HideInEditorMode] [SerializeField]
        protected bool HasMoved;


        [SerializeField] protected MMFeedbacks MoveFeedbacks;
        [SerializeField] protected MMFeedbacks CaptureFeedbacks;
        [SerializeField] protected MMFeedbackPosition PositionFeedback;


        private Grid _grid;


        protected PieceManager PieceManager;


        private SpriteRenderer _renderer;


        // Set at run-time.
        [ReadOnly] [HideInEditorMode] public Vector2Int Position { private set; get; }


        [ReadOnly]
        [HideInEditorMode]
        [SerializeField]
        public PieceColorEnum Color { private set; get; } = PieceColorEnum.Black;


        [ReadOnly] [HideInEditorMode] public uint PieceID { private set; get; }


        private void Awake()
        {
            //We grab the necessary components that we need off of the game object.
            _renderer = GetComponent<SpriteRenderer>();

            //We check to make sure that everything was configured correctly.
            if (_blackSprite == null)
                Debug.LogError("Black Piece Sprite Not Configured In Editor. Please Fix This...", this);

            if (_whiteSprite == null)
                Debug.LogError("White Piece Sprite Not Configured In Editor. Please Fix This...", this);

            if (_renderer == null)
                Debug.LogError("Game piece is missing a SpriteRenderer. Piece might not render. Please Fix This...",
                    this);
        }


        private void OnMouseDown()
        {
            PieceManager.ShowValidMoves(this);
        }

        /// <summary>
        ///     Called when this piece is created to setup all parameters correctly.
        /// </summary>
        /// <param name="pieceManager"></param>
        /// <param name="grid"></param>
        /// <param name="pieceID"></param>
        /// <param name="piecePosition"></param>
        /// <param name="pieceColor"></param>
        public void Setup(PieceManager pieceManager, Grid grid, uint pieceID, Vector2Int piecePosition,
            PieceColorEnum pieceColor)
        {
            PieceManager = pieceManager;
            _grid = grid;
            PieceID = pieceID;
            Position = piecePosition;
            Color = pieceColor;


            _renderer.sprite = Color == PieceColorEnum.Black ? _blackSprite : _whiteSprite;
        }


        /// <summary>
        ///     Returns all possible moves for this piece.
        /// </summary>
        /// <returns></returns>
        public virtual Vector2Int[] GetLegalMoves(Vector2Int boardSize)
        {
            var legalMoves = new List<Vector2Int> {Position + new Vector2Int(1, 1)};

            return legalMoves.ToArray();
        }

        /// <summary>
        ///     Sets the position of this piece.
        /// </summary>
        /// <param name="movePosition">The position to move to.</param>
        public void Move(Vector2Int movePosition)
        {
            if (PieceManager.UpdatePieceLocation(this, Position, movePosition))
            {
                if (MoveFeedbacks != null)
                {
                    PositionFeedback.DestinationPosition =
                        _grid.GetCellCenterWorld((Vector3Int) movePosition) + Vector3.back;
                    MoveFeedbacks.PlayFeedbacks();
                }
                else
                {
                    transform.position = _grid.GetCellCenterWorld((Vector3Int) movePosition) + Vector3.back;
                }

                HasMoved = true;
                Position = movePosition;
            }
            else
            {
                Debug.Log("Unable to make invalid move");
            }
        }

        public virtual void OnCaptured(uint capturerID)
        {
            CaptureFeedbacks?.PlayFeedbacks();
            Debug.Log($"{capturerID} Captured Piece: {PieceID}");
            Destroy(gameObject);
        }
    }
}