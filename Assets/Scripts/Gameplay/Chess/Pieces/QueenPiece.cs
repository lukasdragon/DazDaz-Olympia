using System.Collections.Generic;
using UnityEngine;

namespace olympia.Gameplay.Chess.Pieces
{
    public class QueenPiece : ChessPiece
    {
        public override Vector2Int[] GetLegalMoves(Vector2Int boardSize)
        {
            //TODO: Essentially a rook and bishop. Once GetLegalMoves is refactored to be static, this should call and return a combination of rook and bishop.

            var legalMoves = new List<Vector2Int>();

            ChessPiece piece = null;


            //Forward
            for (var y = 1; y < boardSize.y - Position.y; y++)
            {
                var pos = new Vector2Int(Position.x, Position.y + y);
                if (PieceManager.GetPieceAtPosition(pos, out piece))
                {
                    if (piece.Color != Color) legalMoves.Add(pos);

                    break;
                }

                legalMoves.Add(pos);

                piece = null;
            }

            //Backwards
            for (var y = 1; y < Position.y + 1; y++)
            {
                var pos = new Vector2Int(Position.x, Position.y - y);
                if (PieceManager.GetPieceAtPosition(pos, out piece))
                {
                    if (piece.Color != Color) legalMoves.Add(pos);

                    break;
                }

                legalMoves.Add(pos);

                piece = null;
            }


            //Right
            for (var x = 1; x < boardSize.x - Position.x; x++)
            {
                var pos = new Vector2Int(Position.x + x, Position.y);
                if (PieceManager.GetPieceAtPosition(pos, out piece))
                {
                    if (piece.Color != Color) legalMoves.Add(pos);

                    break;
                }

                legalMoves.Add(pos);

                piece = null;
            }


            //Left
            for (var x = 1; x < Position.x + 1; x++)
            {
                var pos = new Vector2Int(Position.x - x, Position.y);

                if (PieceManager.GetPieceAtPosition(pos, out piece))
                {
                    if (piece.Color != Color) legalMoves.Add(pos);

                    break;
                }

                legalMoves.Add(pos);

                piece = null;
            }

            //Forward right
            for (var i = 1; i < boardSize.y - Position.y; i++)
            {
                var pos = new Vector2Int(Position.x + i, Position.y + i);


                if (pos.x >= boardSize.x) break;


                if (PieceManager.GetPieceAtPosition(pos, out piece))
                {
                    if (piece.Color != Color) legalMoves.Add(pos);

                    break;
                }

                legalMoves.Add(pos);
            }

            //Forward Left
            for (var i = 1; i < boardSize.y - Position.y; i++)
            {
                var pos = new Vector2Int(Position.x - i, Position.y + i);

                if (pos.x < 0) break;

                if (PieceManager.GetPieceAtPosition(pos, out piece))
                {
                    if (piece.Color != Color) legalMoves.Add(pos);

                    break;
                }

                legalMoves.Add(pos);
            }


            //Backwards Right
            for (var i = 1; i < Position.y + 1; i++)
            {
                var pos = new Vector2Int(Position.x + i, Position.y - i);

                if (pos.x >= boardSize.x) break;

                if (PieceManager.GetPieceAtPosition(pos, out piece))
                {
                    if (piece.Color != Color) legalMoves.Add(pos);

                    break;
                }

                legalMoves.Add(pos);
            }


            //Backwards Left
            for (var i = 1; i < Position.y + 1; i++)
            {
                var pos = new Vector2Int(Position.x - i, Position.y - i);

                if (pos.x < 0) break;

                if (PieceManager.GetPieceAtPosition(pos, out piece))
                {
                    if (piece.Color != Color) legalMoves.Add(pos);

                    break;
                }

                legalMoves.Add(pos);
            }

            return legalMoves.ToArray();
        }
    }
}