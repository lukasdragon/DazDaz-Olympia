using olympia.Gameplay.Chess.Pieces;
using UnityEngine;

namespace olympia.Gameplay.Chess
{
    [RequireComponent(typeof(Collider2D))]
    public class MoveMarker : MonoBehaviour
    {
        private ChessPiece _chessPiece;
        private PieceManager _manager;

        private Vector2Int _position;

        /// <summary>
        /// OnMouseDown is called by Unity when the user clicks on this MoveMarker.
        /// </summary>
        private void OnMouseDown()
        {
            _manager.CreatePieceMove(_chessPiece, _position);
        }

        /// <summary>
        /// Must be called when this piece is created to ensure that this piece can call back when pressed.
        /// </summary>
        /// <param name="pieceManager">The associated piece manager.</param>
        /// <param name="chessPiece">The associated chess piece.</param>
        /// <param name="position">The position of this move marker.</param>
        public void Setup(PieceManager pieceManager, ChessPiece chessPiece, Vector2Int position)
        {
            _manager = pieceManager;
            _chessPiece = chessPiece;
            _position = position;
        }
    }
}