using System.Collections.Generic;
using olympia.Data;
using olympia.Gameplay.Chess.Pieces;
using olympia.Interfaces;
using Photon.Pun;
using Sirenix.OdinInspector;
using Unity.Mathematics;
using UnityEngine;
using Zenject;

namespace olympia.Gameplay.Chess
{
    public class PieceManager : MonoBehaviourPunCallbacks
    {
        [BoxGroup("Board Setup")] [Required] [SerializeField]
        private Grid _grid;

        [BoxGroup("Board Setup")] [Required] [SerializeField]
        private GameObject _pieceHolder;


        private readonly Dictionary<uint, ChessPiece> _pieceIDs = new Dictionary<uint, ChessPiece>();
        private readonly Dictionary<Vector2Int, ChessPiece> _piecePositions = new Dictionary<Vector2Int, ChessPiece>();
        private BoardManager _boardManager;


        private PunChessDataManager _chessDataManager;
        private IMatchManager _matchManager;

        [Inject]
        public void Init(PunChessDataManager punChessDataManager, BoardManager boardManager,
            IMatchManager matchManager)
        {
            _chessDataManager = punChessDataManager;
            _boardManager = boardManager;
            _matchManager = matchManager;
        }

        private void Awake()
        {
            if (_chessDataManager == null)
                Debug.LogError(
                    "ChessDataManager has not been injected. Please ensure that the dependency injector is correctly configured.");
        }

        public override void OnEnable()
        {
            base.OnEnable();
            _matchManager.MatchStart += MatchManagerOnMatchStart;
            _matchManager.MoveReceived += MatchManagerOnMoveReceived;
            _matchManager.Cleanup += MatchManagerOnCleanup;
        }


        public override void OnDisable()
        {
            base.OnDisable();
            _matchManager.MatchStart -= MatchManagerOnMatchStart;
            _matchManager.MoveReceived += MatchManagerOnMoveReceived;
            _matchManager.Cleanup -= MatchManagerOnCleanup;
        }

        private void MatchManagerOnMatchStart()
        {
            var sequence = GenerateSequence(_chessDataManager.BoardSize.x, _chessDataManager.BoardSize.y);
            PlacePiecesFromSequence(sequence);
        }

        private void MatchManagerOnMoveReceived(ChessMove move)
        {
            if (VerifyPieceMove(move))
            {
                _pieceIDs.TryGetValue(move.PieceId, out var piece);
                piece.Move(move.EndPosition);
            }
            else
            {
                Debug.LogWarning("Received an invalid chessMove...");
            }
        }

        private void MatchManagerOnCleanup()
        {
            _pieceIDs.Clear();
            _piecePositions.Clear();

            for (var i = 0; i < _pieceHolder.transform.childCount; i++)
            {
                Destroy(_pieceHolder.transform.GetChild(i).gameObject);
            }
        }


        /// <summary>
        ///     Generates the placement sequence of any given chess board based upon its X and Y size.
        /// </summary>
        /// <param name="sizeX">The X (alpha) axis of the board.</param>
        /// <param name="sizeY">The Y (numeral) axis of the board.</param>
        /// <returns>The corresponding chess sequence</returns>
        private static PieceStruct[,] GenerateSequence(int sizeX, int sizeY)
        {
            var sequence = new PieceStruct[sizeX, sizeY];


            for (var y = 0; y < sizeY; y++)
            {
                //We assign our pawns to the front line
                if (y == 1 || y == sizeY - 2)
                    for (var x = 0; x < sizeX; x++)
                        sequence[x, y] = new PieceStruct(PieceTypeEnum.Pawn,
                            y == 1 ? PieceColorEnum.White : PieceColorEnum.Black);

                //We assign the rest of our pieces
                if (y == 0 || y == sizeY - 1)
                    for (var x = 0; x < sizeX; x++)
                        if (x == sizeX / 2 - 1) //Our Queens
                            sequence[x, y] = new PieceStruct(PieceTypeEnum.Queen,
                                y == 0 ? PieceColorEnum.White : PieceColorEnum.Black);
                        else if (x == sizeX / 2) //Our Kings
                            sequence[x, y] = new PieceStruct(PieceTypeEnum.King,
                                y == 0 ? PieceColorEnum.White : PieceColorEnum.Black);
                        else if (x == 2 || x == sizeX - 3) //Our Bishops
                            sequence[x, y] = new PieceStruct(PieceTypeEnum.Bishop,
                                y == 0 ? PieceColorEnum.White : PieceColorEnum.Black);
                        else if (x == 1 || x == sizeX - 2) //Our Knights
                            sequence[x, y] = new PieceStruct(PieceTypeEnum.Knight,
                                y == 0 ? PieceColorEnum.White : PieceColorEnum.Black);
                        else if (x == 0 || x == sizeX - 1) //Our Rooks
                            sequence[x, y] = new PieceStruct(PieceTypeEnum.Rook,
                                y == 0 ? PieceColorEnum.White : PieceColorEnum.Black);
                        else // We fill any remaining spots with pawns. Might have to come up with different gameplay mechanics if larger boards are ever used seriously.
                            sequence[x, y] = new PieceStruct(PieceTypeEnum.Pawn,
                                y == 0 ? PieceColorEnum.White : PieceColorEnum.Black);
            }

            return sequence;
        }

        /// <summary>
        ///     Takes a board sequence and places it on the board.
        /// </summary>
        /// <param name="sequence">The sequence in which the pieces are the be arranged.</param>
        private void PlacePiecesFromSequence(PieceStruct[,] sequence)
        {
            Debug.Log("Placing pieces from sequence");
            _pieceIDs.Clear();
            _piecePositions.Clear();
            uint pieceID = 0;

            for (var x = 0; x < sequence.GetLength(0); x++)
            for (var y = 0; y < sequence.GetLength(1); y++)
            {
                var piece = sequence[x, y];

                if (piece.PieceType == PieceTypeEnum.Empty) continue;

                var cellPosition = new Vector3Int(x, y, 1);
                var worldPosition = _grid.GetCellCenterWorld(cellPosition) + Vector3.back;

                if (_chessDataManager.ChessPieces.ContainsKey(piece.PieceType))
                {
                    var chessPiece = Instantiate(_chessDataManager.ChessPieces[piece.PieceType], worldPosition,
                        quaternion.identity,
                        _pieceHolder.transform);
                    chessPiece.transform.localScale = _grid.cellSize;
                    chessPiece.Setup(this, _grid, pieceID, new Vector2Int(x, y),
                        piece.Color);

                    _pieceIDs.Add(pieceID, chessPiece);
                    _piecePositions.Add(new Vector2Int(x, y), chessPiece);

                    pieceID++;
                }
                else
                {
                    Debug.LogError(
                        $"A Chess piece is missing from the ChessPiece Data. Please ensure that there is a {piece.PieceType} present.");
                }
            }
        }

        /// <summary>
        /// Creates move markers for all possible moves for a given chessPiece.
        /// </summary>
        /// <param name="chessPiece">The chessPiece to display possible moves for.</param>
        public void ShowValidMoves(ChessPiece chessPiece)
        {
            if (_matchManager.IsLocalPlayerTurn && chessPiece.Color == _matchManager.LocalPlayerColor)
            {
                var moves = chessPiece.GetLegalMoves(_chessDataManager.BoardSize);
                _boardManager.ShowMovePositions(this, chessPiece, moves);
            }
            else
            {
                Debug.Log("Not our turn.");
            }
        }

        /// <summary>
        /// Creates and sends a piece movement.
        /// </summary>
        /// <param name="chessPiece">The ChessPiece to move.</param>
        /// <param name="movePosition">The position to move the ChessPiece.</param>
        public void CreatePieceMove(ChessPiece chessPiece, Vector2Int movePosition)
        {
            _boardManager.ClearMoveMarkers();

            var chessMove = new ChessMove(chessPiece.PieceID, chessPiece.Position, movePosition);
            if (!VerifyPieceMove(chessMove))
                Debug.Log("Invalid Chess Move Requested...");
            else
                _matchManager.SubmitMove(chessMove);
        }

        /// <summary>
        /// Verify whether or not a piece move is valid.
        /// </summary>
        /// <param name="chessMove">The ChessMove to validate.</param>
        /// <returns>Whether or not the piece move is valid.</returns>
        private bool VerifyPieceMove(ChessMove chessMove)
        {
            if (!_pieceIDs.TryGetValue(chessMove.PieceId, out var piece)) return false;
            if (piece.Position != chessMove.StartPosition) return false;


            var legalMoves = new List<Vector2Int>(piece.GetLegalMoves(_chessDataManager.BoardSize));

            return legalMoves.Contains(chessMove.EndPosition);
        }


        /// <summary>
        /// Updates the logical board position of a piece.
        /// </summary>
        /// <param name="piece">The piece to update location for.</param>
        /// <param name="oldPosition">The old position of the piece.</param>
        /// <param name="newPosition">The new position of the piece.</param>
        /// <returns>Whether we were able to update the piece position.</returns>
        public bool UpdatePieceLocation(ChessPiece piece, Vector2Int oldPosition, Vector2Int newPosition)
        {
            if (_piecePositions.ContainsKey(newPosition))
                if (_piecePositions.TryGetValue(newPosition, out var otherPiece))
                {
                    if (otherPiece.Color != piece.Color)
                    {
                        otherPiece.OnCaptured(piece.PieceID);
                        _piecePositions.Remove(newPosition);
                    }
                    else
                    {
                        return false;
                    }
                }

            _piecePositions.Remove(oldPosition);
            _piecePositions.Add(newPosition, piece);
            return true;
        }

        /// <summary>
        /// Called from the local king piece when it's captured. Ends the game.
        /// </summary>
        public void KingCaptured()
        {
            _matchManager.EndGame(EndGameEventType.Loss);
        }

        /// <summary>
        /// Gets a ChessPiece by its ID.
        /// </summary>
        /// <param name="pieceID">The ID of the ChessPiece.</param>
        /// <param name="piece">A variable to hold the gotten ChessPiece.</param>
        /// <returns>Whether the ChessPiece was able to be retrieved or not.</returns>
        public bool GetPieceByID(uint pieceID, out ChessPiece piece)
        {
            var exists = _pieceIDs.TryGetValue(pieceID, out piece);
            return exists;
        }

        /// <summary>
        /// Gets a ChessPiece by its position.
        /// </summary>
        /// <param name="position">The logical position of a ChessPiece.</param>
        /// <param name="piece">The ChessPiece that exists at that location.</param>
        /// <returns>Whether we were able to retrieve a ChessPiece from a location.</returns>
        public bool GetPieceAtPosition(Vector2Int position, out ChessPiece piece)
        {
            var exists = _piecePositions.TryGetValue(position, out piece);
            return exists;
        }

        /// <summary>
        /// Gets whether a ChessPiece exists at a position.
        /// </summary>
        /// <param name="position">The position to check for a ChessPiece.</param>
        /// <returns>Whether or not a ChessPiece exists at that location.</returns>
        public bool IsPieceAtPosition(Vector2Int position)
        {
            return _piecePositions.ContainsKey(position);
        }
    }
}