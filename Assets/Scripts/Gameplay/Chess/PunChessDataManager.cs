﻿using System;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using olympia.Data;
using olympia.Gameplay.Chess.Pieces;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.Scripting;
using UnityEngine.Tilemaps;

namespace olympia.Gameplay.Chess
{
    /// <summary>
    ///     This class is responsible for providing and synchronizing the static data necessary to run a match of chess.
    ///     This includes providing the board size, pieces, and graphics.
    /// </summary>
    [Serializable]
    [Preserve]
    public class PunChessDataManager : IMatchmakingCallbacks, IDisposable
    {
        private readonly ChessBoardConfiguration _chessBoardConfiguration;

        public PunChessDataManager(ChessBoardConfiguration chessBoardConfiguration)
        {
            _chessBoardConfiguration = chessBoardConfiguration;
        }

        public Dictionary<PieceTypeEnum, ChessPiece> ChessPieces => _chessBoardConfiguration.ChessPieces;

        public Vector2Int BoardSize { private set; get; }

        public TileBase BlackTile => _chessBoardConfiguration.BlackTile;
        public TileBase WhiteTile => _chessBoardConfiguration.WhiteTile;


        public MoveMarker MoveMarker => _chessBoardConfiguration.MoveMarker;

        public void Dispose()
        {
            Debug.Log("Disposing the PunChess DataManager");
            //When we finish up, we must always remove our registration for the callbacks.
            PhotonNetwork.RemoveCallbackTarget(this);
        }


        //We're using this to synchronize game data when we join the room.
        public void OnJoinedRoom()
        {
            // We ensure that we start off with the configured board size of our client. 
            BoardSize = _chessBoardConfiguration.BoardSize;

            //If we own the match we set the board size properties for the room. Otherwise, we grab them.
            if (PhotonNetwork.IsMasterClient)
            {
                var customProperties = new Hashtable {{"MAP_X", BoardSize.x}, {"MAP_Y", BoardSize.y}};
                PhotonNetwork.CurrentRoom.SetCustomProperties(customProperties);
            }
            else
            {
                var sizeX = 8;
                var sizeY = 8;
                //We grab the X-size of the board from the room properties.
                if (PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue("MAP_X", out var mapXProperty))
                    //Cast the object back to an int.
                    sizeX = (int) mapXProperty;

                //We grab the Y-size of the board from the room properties.
                if (PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue("MAP_Y", out var mapYProperty))
                    sizeY = (int) mapYProperty;

                BoardSize = new Vector2Int(sizeX, sizeY);
            }
        }

        //Clean up our data when we leave the room.
        public void OnLeftRoom()
        {
            BoardSize = _chessBoardConfiguration.BoardSize;
        }

        public void OnFriendListUpdate(List<FriendInfo> friendList)
        {
        }

        public void OnCreatedRoom()
        {
        }

        public void OnCreateRoomFailed(short returnCode, string message)
        {
        }

        public void OnJoinRoomFailed(short returnCode, string message)
        {
        }

        public void OnJoinRandomFailed(short returnCode, string message)
        {
        }


        public void Initialize()
        {
            Debug.Log("Initializing the PunChess DataManager");
            //We have to register ourselves to receive the PUN callbacks.
            //We use these callbacks to know when we should synchronize data.
            PhotonNetwork.AddCallbackTarget(this);

            //Used here to verify integrity of all game configuration.
            if (BlackTile == null || WhiteTile == null)
                Debug.LogError(
                    "Please ensure that the BlackTile and WhiteTile properties are correctly set in the editor.",
                    _chessBoardConfiguration);

            if (MoveMarker == null) Debug.LogError("Please ensure that you have correctly set a Move Marker object.");
        }
    }
}