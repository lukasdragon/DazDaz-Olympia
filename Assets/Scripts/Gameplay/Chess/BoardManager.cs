using System.Collections.Generic;
using olympia.Gameplay.Chess.Pieces;
using olympia.Interfaces;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Tilemaps;
using Zenject;

namespace olympia.Gameplay.Chess
{
    public class BoardManager : MonoBehaviour
    {
        [BoxGroup("Board Setup")]
        [Required]
        [SerializeField]
        [InfoBox("The Tilemap used to generate the chessboard visual.")]
        private Tilemap _chessBoardTileMap;

        [BoxGroup("Board Setup")] [Required] [SerializeField] [InfoBox("The GameObject that holds all move markers.")]
        private GameObject _moveMarkerHolder;


        [HideInEditorMode] [ReadOnly] [SerializeField]
        private List<GameObject> _moveMarkers = new List<GameObject>();


        private PunChessDataManager _chessDataManager;
        private IMatchManager _matchManager;

        [Inject]
        public void Init(PunChessDataManager chessDataManager, IMatchManager chessMatchManager)
        {
            _chessDataManager = chessDataManager;
            _matchManager = chessMatchManager;
        }


        private void Awake()
        {
            //We double check that all properties have been correctly set.
            if (_chessBoardTileMap == null)
                Debug.LogError("Please ensure that the TileMap properties have been correctly set in the editor.",
                    this);

            if (_chessDataManager == null)
                Debug.LogError(
                    "ChessDataManager has not been injected. Please ensure that the dependency injector has been correctly configured.");
        }


        private void OnEnable()
        {
            _matchManager.MatchStart += MatchManagerOnMatchStart;
            _matchManager.Cleanup += MatchManagerOnCleanup;
        }

        private void OnDisable()
        {
            _matchManager.MatchStart -= MatchManagerOnMatchStart;
            _matchManager.Cleanup -= MatchManagerOnCleanup;
        }

        private void MatchManagerOnMatchStart()
        {
            GenerateBoard(_chessBoardTileMap, _chessDataManager.BlackTile, _chessDataManager.WhiteTile,
                _chessDataManager.BoardSize.x, _chessDataManager.BoardSize.y);
        }

        private void MatchManagerOnCleanup()
        {
            ClearMoveMarkers();
            _chessBoardTileMap.ClearAllTiles();
        }


        /// <summary>
        ///     Displays move markers at the supplied positions.
        /// </summary>
        /// <param name="pieceManager"></param>
        /// <param name="chessPiece"></param>
        /// <param name="positions"></param>
        public void ShowMovePositions(PieceManager pieceManager, ChessPiece chessPiece, Vector2Int[] positions)
        {
            ClearMoveMarkers();
            foreach (var position in positions) AddMoveMarker(pieceManager, chessPiece, position);
        }

        /// <summary>
        ///     Removes the move markers from the board.
        /// </summary>
        public void ClearMoveMarkers()
        {
            foreach (var moveMarker in _moveMarkers) Destroy(moveMarker);

            _moveMarkers.Clear();
        }


        /// <summary>
        /// Adds a move marker to the board
        /// </summary>
        /// <param name="pieceManager">The associated piece manager.</param>
        /// <param name="chessPiece">The associated chess piece.</param>
        /// <param name="position">The position at which to place the move marker.</param>
        private void AddMoveMarker(PieceManager pieceManager, ChessPiece chessPiece, Vector2Int position)
        {
            var marker = Instantiate(_chessDataManager.MoveMarker, _moveMarkerHolder.transform, true);
            _moveMarkers.Add(marker.gameObject);

            var markerTransform = marker.transform;
            var boardLayoutGrid = _chessBoardTileMap.layoutGrid;


            markerTransform.position =
                boardLayoutGrid.GetCellCenterWorld((Vector3Int) position) + new Vector3(0, 0, -3);
            markerTransform.localScale = boardLayoutGrid.cellSize;


            marker.Setup(pieceManager, chessPiece, position);
        }


        /// <summary>
        ///     Generates a 'chess' board pattern on a Tilemap
        /// </summary>
        /// <param name="tilemap">The tilemap to generate the pattern on.</param>
        /// <param name="blackTile">The TileBase that represents a black square.</param>
        /// <param name="whiteTile">The TileBase that represents a white square.</param>
        /// <param name="sizeX">The size of the board to generate along the X-Axis.</param>
        /// <param name="sizeY">The size of the board to generate along the Y-Axis.</param>
        private static void GenerateBoard(Tilemap tilemap, TileBase blackTile, TileBase whiteTile, int sizeX, int sizeY)
        {
            //We clear the tilemap so that we're ready to generate a new board.
            //It's usually already cleared if we properly left the last match,
            //but it's always better to be safe than sorry.
            tilemap.ClearAllTiles();

            for (var x = 0; x < sizeX; x++)
            for (var y = 0; y < sizeY; y++)
                tilemap.SetTile(new Vector3Int(x, y, 0), (x + y) % 2 == 0 ? blackTile : whiteTile);
        }

        /// <summary>
        ///     Sets the tiles of a given tilemap with the supplied tile at a the supplied positions.
        /// </summary>
        /// <param name="tilemap"></param>
        /// <param name="tile"></param>
        /// <param name="positions"></param>
        private static void SetTileAtPositions(Tilemap tilemap, TileBase tile, Vector2Int[] positions)
        {
            foreach (var position in positions) tilemap.SetTile((Vector3Int) position, tile);
        }
    }
}