using System.Collections.Generic;
using ExitGames.Client.Photon;
using olympia.Data;
using olympia.Interfaces;
using olympia.Networking;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.Scripting;
using Random = UnityEngine.Random;

namespace olympia.Gameplay.Chess
{
    [Preserve]
    public sealed class ChessMatchManager : MonoBehaviourPunCallbacks, IOnEventCallback, IMatchManager
    {
        /// <summary>
        /// Called when a match starts.
        /// </summary>
        public event SimpleEventHandler MatchStart;

        /// <summary>
        /// Called when a match ends.
        /// </summary>
        public event SimpleEventHandler<EndGameEventType> MatchEnd;

        /// <summary>
        /// Called when a turn for either player starts.
        /// </summary>
        public event SimpleEventHandler TurnStart;

        /// <summary>
        /// Called when we receive a move command.
        /// </summary>
        public event SimpleEventHandler<ChessMove> MoveReceived;

        /// <summary>
        /// Gets called whenever the game state should be reset to a clean slate. I.e., after a match.
        /// </summary>
        public event SimpleEventHandler Cleanup;


        public bool HasStarted { get; private set; }
        public List<ChessMove> MoveList { get; private set; } = new List<ChessMove>(40);

        public int TurnCount => MoveList.Count;

        public PieceColorEnum LocalPlayerColor
        {
            get
            {
                if (PhotonNetwork.LocalPlayer.CustomProperties.ContainsKey("team"))
                {
                    return (PieceColorEnum) PhotonNetwork.LocalPlayer.CustomProperties["team"];
                }

                return PieceColorEnum.Spectator;
            }
        }

        public bool IsLocalPlayerTurn
        {
            get
            {
                if (TurnCount % 2 == 0)
                {
                    if (LocalPlayerColor == PieceColorEnum.White)
                    {
                        return true;
                    }
                }
                else
                {
                    if (LocalPlayerColor == PieceColorEnum.Black)
                    {
                        return true;
                    }
                }

                return false;
            }
        }


        public void OnEvent(EventData photonEvent)
        {
            var eventCode = photonEvent.Code;
            switch (eventCode)
            {
                case EventCodes.StartGameEventCode:
                    if (HasStarted != true)
                    {
                        OnMatchStart();
                        HasStarted = true;
                    }

                    break;

                case EventCodes.PieceMoveEvent:
                    MoveList.Add((ChessMove) photonEvent.CustomData);
                    OnTurnReceived((ChessMove) photonEvent.CustomData);
                    OnTurnStart();
                    break;

                case EventCodes.GameStatusEndEvent:
                    var data = (EndGameEventType) photonEvent.CustomData;

                    Debug.Log(data);

                    //If this event was not send from the local client,
                    //we reverse the event type so that our local events receive the correct state according to our perspective.
                    if (!photonEvent.Sender.Equals(PhotonNetwork.LocalPlayer.ActorNumber))
                    {
                        data = data switch
                        {
                            EndGameEventType.Win => EndGameEventType.Loss,
                            EndGameEventType.Loss => EndGameEventType.Win,
                            EndGameEventType.Forfeit => EndGameEventType.Win,
                            _ => data
                        };
                    }


                    OnMatchEnd(data);
                    EndMatch(data);


                    break;
            }
        }

        private void EndMatch(EndGameEventType photonEventCustomData)
        {
            PhotonNetwork.LeaveRoom();
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            if (PhotonNetwork.IsMasterClient && PhotonNetwork.CurrentRoom.PlayerCount >= 2)
            {
                StartMatch(newPlayer);
            }
        }

        public override void OnLeftRoom()
        {
            OnCleanup();

            MoveList.Clear();
            HasStarted = false;
        }


        private void StartMatch(Player opponent)
        {
            if (!HasStarted)
            {
                Debug.Log("Starting match!");
                //Assigning team colors

                var playerOneColor = Random.Range(0, 1) == 1 ? PieceColorEnum.White : PieceColorEnum.Black;
                var playerTwoColor =
                    playerOneColor == PieceColorEnum.Black ? PieceColorEnum.White : PieceColorEnum.Black;


                PhotonNetwork.CurrentRoom.GetPlayer(0, true)
                    .SetCustomProperties(new Hashtable {{"team", playerOneColor}});
                PhotonNetwork.CurrentRoom.GetPlayer(opponent.ActorNumber, true)
                    .SetCustomProperties(new Hashtable {{"team", playerTwoColor}});

                //Sending event to start match

                Debug.Log("Sending event to start match...");
                var raiseEventOptions = new RaiseEventOptions
                {
                    Receivers = ReceiverGroup.All,
                    CachingOption = EventCaching.AddToRoomCacheGlobal
                };
                PhotonNetwork.RaiseEvent(EventCodes.StartGameEventCode, null, raiseEventOptions,
                    SendOptions.SendReliable);
            }
        }

        public void SubmitMove(ChessMove move)
        {
            var raiseEventOptions = new RaiseEventOptions
            {
                Receivers = ReceiverGroup.All,
                CachingOption = EventCaching.AddToRoomCache
            };
            PhotonNetwork.RaiseEvent(EventCodes.PieceMoveEvent, move, raiseEventOptions, SendOptions.SendReliable);
        }

        public void EndGame(EndGameEventType reason)
        {
            var raiseEventOptions = new RaiseEventOptions
            {
                Receivers = ReceiverGroup.All,
                CachingOption = EventCaching.AddToRoomCache
            };
            PhotonNetwork.RaiseEvent(EventCodes.GameStatusEndEvent, reason, raiseEventOptions,
                SendOptions.SendReliable);
        }

        private void OnTurnStart()
        {
            TurnStart?.Invoke();
        }

        private void OnMatchStart()
        {
            MatchStart?.Invoke();
        }

        private void OnMatchEnd(EndGameEventType reason)
        {
            MatchEnd?.Invoke(reason);
        }

        private void OnTurnReceived(ChessMove move)
        {
            MoveReceived?.Invoke(move);
        }

        private void OnCleanup()
        {
            Cleanup?.Invoke();
        }
    }
}