using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace olympia.UI
{
    [RequireComponent(typeof(TMP_Text))]
    public class GameVersionText : MonoBehaviour
    {
        [Required] [SerializeField] private TMP_Text _versionText;

        // Start is called before the first frame update
        void Start()
        {
            _versionText.text = $"{Application.unityVersion}.{Application.version}";
        }
    }
}