using olympia.Networking;
using UnityEngine;
using Zenject;

namespace olympia.UI
{
    public class MainMenuUI : MonoBehaviour
    {
        [SerializeField] private GameObject _connectingToServersScreen;
        private NetworkLauncher _networkLauncher;

        private void Start()
        {
            if (_connectingToServersScreen != null) _connectingToServersScreen.SetActive(false);
        }

        public void StartNewGame(bool passCodeRestricted = false)
        {
            _networkLauncher.CreateRoom(passCodeRestricted);
            OpenScreen(_connectingToServersScreen);
        }


        public void FindMatch()
        {
            _networkLauncher.JoinRandomRoom();
            OpenScreen(_connectingToServersScreen);
        }

        public void JoinMatchByCode(string code)
        {
            if (string.IsNullOrEmpty(code)) return;
            _networkLauncher.JoinRoomByCode(code);
            OpenScreen(_connectingToServersScreen);
        }

        private void OpenScreen(GameObject screen)
        {
            if (screen != null) screen.SetActive(true);
        }

        [Inject]
        public void Init(NetworkLauncher networkLauncher)
        {
            _networkLauncher = networkLauncher;
        }
    }
}