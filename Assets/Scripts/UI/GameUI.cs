using olympia.Data;
using olympia.Interfaces;
using TMPro;
using UnityEngine;
using Zenject;

namespace olympia.UI
{
    public class GameUI : MonoBehaviour
    {
        private IMatchManager _chessMatchManager;

        [SerializeField] private TMP_Text _turnText;
        [SerializeField] private TMP_Text _playerColorText;
        [SerializeField] private TMP_Text _turnStatusText;


        [SerializeField] private TMP_Text _matchEndStatus;

        [Inject]
        public void Init(IMatchManager chessMatchManager)
        {
            _chessMatchManager = chessMatchManager;
        }

        private void OnEnable()
        {
            _chessMatchManager.MatchStart += ChessMatchManagerOnMatchStart;
            _chessMatchManager.TurnStart += ChessMatchManagerOnTurnStart;
            _chessMatchManager.MatchEnd += ChessMatchManagerOnMatchEnd;
        }

        private void OnDisable()
        {
            _chessMatchManager.MatchStart -= ChessMatchManagerOnMatchStart;
            _chessMatchManager.TurnStart -= ChessMatchManagerOnTurnStart;
            _chessMatchManager.MatchEnd -= ChessMatchManagerOnMatchEnd;
        }

        private void ChessMatchManagerOnMatchStart()
        {
            _playerColorText.text = _chessMatchManager.LocalPlayerColor switch
            {
                PieceColorEnum.Black => "Black",
                PieceColorEnum.White => "White",
                PieceColorEnum.Spectator => "Spectator",
                _ => _playerColorText.text
            };

            _turnStatusText.text = _chessMatchManager.IsLocalPlayerTurn ? "Your Turn" : "Opponents Turn";

            _turnText.text = "Turn 1";
        }

        private void ChessMatchManagerOnTurnStart()
        {
            _turnText.text = $"Turn {_chessMatchManager.TurnCount}";
            _turnStatusText.text = _chessMatchManager.IsLocalPlayerTurn ? "Your Turn" : "Opponents Turn";
        }

        private void ChessMatchManagerOnMatchEnd(EndGameEventType data)
        {
            _matchEndStatus.text = data switch
            {
                EndGameEventType.Win => "You're a Winner!",
                EndGameEventType.Loss => "You're a Loser!",
                EndGameEventType.Forfeit => "Forfeiture!",
                EndGameEventType.Draw => "You've Entered a Draw!",
                _ => _matchEndStatus.text
            };
        }

        public void Forfeit()
        {
            _chessMatchManager.EndGame(EndGameEventType.Forfeit);
        }
    }
}