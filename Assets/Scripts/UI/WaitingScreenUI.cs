using Photon.Pun;
using TMPro;
using UnityEngine;

namespace olympia.UI
{
    public class WaitingScreenUI : MonoBehaviour
    {
        [SerializeField] private TMP_Text _roomCodeText;
        
        
        private void OnEnable()
        {
            if (PhotonNetwork.CurrentRoom.IsVisible)
            {
                _roomCodeText.gameObject.SetActive(false);
            }
            else
            {
                _roomCodeText.gameObject.SetActive(true);
                _roomCodeText.text = $"Room Code: {PhotonNetwork.CurrentRoom.Name}";
            }
        }

        private void OnDisable()
        {
            _roomCodeText.gameObject.SetActive(false);
            _roomCodeText.text = "Room Code: ####";
        }
    }
}