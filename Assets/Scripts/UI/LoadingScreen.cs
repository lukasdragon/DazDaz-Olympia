using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace olympia.UI
{
    public class LoadingScreen : MonoBehaviour
    {
        [SerializeField] [ListDrawerSettings(Expanded = true)]
        private SceneReference[] _scenesToLoad;

        private void Start()
        {
            for (var i = 0; i < _scenesToLoad.Length; i++)
                SceneManager.LoadSceneAsync(_scenesToLoad[i], i == 0 ? LoadSceneMode.Single : LoadSceneMode.Additive);
        }
    }
}