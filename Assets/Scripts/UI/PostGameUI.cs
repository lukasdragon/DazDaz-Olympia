using olympia.Data;
using olympia.Gameplay.Chess;
using TMPro;
using UnityEngine;
using Zenject;

namespace olympia.UI
{
    public class PostGameUI : MonoBehaviour
    {
        [SerializeField] private TMP_Text _matchStatus;


        private ChessMatchManager _matchManager;


        [Inject]
        public void Init(ChessMatchManager matchManager)
        {
            _matchManager = matchManager;
        }

        private void OnEnable()
        {
            _matchManager.MatchEnd += MatchManagerOnMatchEnd;
        }

        private void OnDisable()
        {
            _matchManager.MatchEnd -= MatchManagerOnMatchEnd;
        }

        private void MatchManagerOnMatchEnd(EndGameEventType data)
        {
            _matchStatus.text = data switch
            {
                EndGameEventType.Win => "Winner!",
                EndGameEventType.Loss => "Loss!",
                EndGameEventType.Forfeit => "Forfeit!",
                EndGameEventType.Draw => "Draw!",
                _ => _matchStatus.text
            };
        }
    }
}