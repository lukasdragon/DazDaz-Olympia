﻿namespace olympia.Data
{
    public enum PieceTypeEnum
    {
        Empty,
        Pawn,
        Bishop,
        Knight,
        Rook,
        Queen,
        King
    }
}