﻿namespace olympia.Data
{
    public enum PieceColorEnum
    {
        Spectator,
        Black,
        White
    }
}