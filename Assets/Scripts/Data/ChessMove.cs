﻿using System;
using UnityEngine;

namespace olympia.Data
{
    public readonly struct ChessMove
    {
        public readonly uint PieceId;
        public readonly Vector2Int StartPosition;
        public readonly Vector2Int EndPosition;

        public ChessMove(uint pieceId, Vector2Int startPosition, Vector2Int endPosition)
        {
            PieceId = pieceId;
            StartPosition = startPosition;
            EndPosition = endPosition;
        }

        public static object Deserialize(byte[] data)
        {
            var pieceID = BitConverter.ToUInt32(data, 0);
            var startPosX = BitConverter.ToInt32(data, 4);
            var startPosY = BitConverter.ToInt32(data, 8);
            var endPosX = BitConverter.ToInt32(data, 12);
            var endPosY = BitConverter.ToInt32(data, 16);

            var result = new ChessMove(pieceID, new Vector2Int(startPosX, startPosY), new Vector2Int(endPosX, endPosY));

            return result;
        }

        public static byte[] Serialize(object customType)
        {
            var c = (ChessMove) customType;
            var data = new byte[20];

            BitConverter.GetBytes(c.PieceId).CopyTo(data, 0);
            BitConverter.GetBytes(c.StartPosition.x).CopyTo(data, 4);
            BitConverter.GetBytes(c.StartPosition.y).CopyTo(data, 8);

            BitConverter.GetBytes(c.EndPosition.x).CopyTo(data, 12);
            BitConverter.GetBytes(c.EndPosition.y).CopyTo(data, 16);


            return data;
        }
    }
}