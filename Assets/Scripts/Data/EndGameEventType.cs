﻿namespace olympia.Data
{
    public enum EndGameEventType
    {
        Invalid,
        Win,
        Loss,
        Forfeit,
        Draw
    }
}