﻿using System.Collections.Generic;
using olympia.Gameplay.Chess;
using olympia.Gameplay.Chess.Pieces;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace olympia.Data
{
    [CreateAssetMenu(fileName = "Chess Board Configuration", menuName = "Data/Chess Board Configuration", order = 1)]
    public class ChessBoardConfiguration : SerializedScriptableObject
    {
        [BoxGroup("Board Visuals")] [Required] [SerializeField]
        public TileBase BlackTile;

        [BoxGroup("Board Visuals")] [Required] [SerializeField]
        public TileBase WhiteTile;

        [BoxGroup("Board Visuals")] [Required] [SerializeField]
        public MoveMarker MoveMarker;

        [ValidateInput("@(BoardSize.x > 0 && BoardSize.y > 0 )",
            "Both values must be greater than 0.")]
        [BoxGroup("Board Gameplay")]
        public readonly Vector2Int BoardSize = new Vector2Int(8, 8);

        [BoxGroup("Board Gameplay")] [Required]
        public readonly Dictionary<PieceTypeEnum, ChessPiece> ChessPieces = new Dictionary<PieceTypeEnum, ChessPiece>();
    }
}