﻿namespace olympia.Data
{
    public readonly struct PieceStruct
    {
        public PieceTypeEnum PieceType { get; }
        public PieceColorEnum Color { get; }

        public PieceStruct(PieceTypeEnum pieceType, PieceColorEnum color)
        {
            PieceType = pieceType;
            Color = color;
        }
    }
}