﻿using System;

namespace olympia.Data
{
    [Serializable]
    public delegate void SimpleEventHandler<in TData>(TData data);

    [Serializable]
    public delegate void SimpleEventHandler();
}