using System;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Events;

namespace olympia.Networking
{
    public class PunUnityEvent : MonoBehaviourPunCallbacks
    {
        [SerializeField] private UnityEvent _start;
        [SerializeField] private UnityEvent _connectedToMaster;
        [SerializeField] private UnityEvent _joinRoom;
        [SerializeField] private UnityEvent<Tuple<short, string>> _joinRoomFailed;
        [SerializeField] private UnityEvent<Tuple<short, string>> _joinRandomRoomFailed;
        [SerializeField] private UnityEvent _leftRoom;


        private void Start()
        {
            _start.Invoke();
        }

        public override void OnConnectedToMaster()
        {
            _connectedToMaster.Invoke();
        }

        public override void OnLeftRoom()
        {
            _leftRoom.Invoke();
        }

        public override void OnJoinedRoom()
        {
            _joinRoom.Invoke();
        }

        public override void OnJoinRoomFailed(short returnCode, string message)
        {
            Debug.Log("Join Room Failed");
            _joinRoomFailed.Invoke(new Tuple<short, string>(returnCode, message));
        }


        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.Log("Join Random Room Failed");
            _joinRandomRoomFailed.Invoke(new Tuple<short, string>(returnCode, message));
        }
    }
}