namespace olympia.Networking
{
    /// <summary>
    /// All the codes used by Photon when sending events.
    /// </summary>
    public static class EventCodes
    {
        public const byte StartGameEventCode = 1;
        public const byte PieceMoveEvent = 2;
        public const byte PieceMoveInvalidEvent = 3;
        public const byte GameStatusEndEvent = 4;
    }
}