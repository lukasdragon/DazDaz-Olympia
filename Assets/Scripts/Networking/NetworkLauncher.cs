using System;
using System.Linq;
using ExitGames.Client.Photon;
using JetBrains.Annotations;
using olympia.Data;
using olympia.Integrations;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.Scripting;
using Zenject;
using Random = System.Random;

namespace olympia.Networking
{
    /// <summary>
    ///     Helps us interface with the Pun2 network.
    /// </summary>
    [UsedImplicitly]
    [Preserve]
    public class NetworkLauncher : IInitializable, IDisposable
    {
        [SerializeField] private readonly string _gameVersion = Application.version;

        private PlayFabManager _playFabManager;

        [Inject]
        public void Injection(PlayFabManager playFabManager)
        {
            _playFabManager = playFabManager;
        }


        /// <summary>
        ///     Equivalent to Unity' start method.
        /// </summary>
        public void Initialize()
        {
            if (!_playFabManager.IsAuthenticated)
            {
                _playFabManager.AuthenticatedEvent += Connect;
            }
            else
            {
                Connect();
            }
        }


        /// <summary>
        ///     Connect to the PUN2 Network.
        /// </summary>
        private void Connect()
        {
            _playFabManager.AuthenticatedEvent -= Connect;
            if (!RegisterCustomTypes())
                Debug.LogError("Unable to register all custom types... This may cause network issues");

            PhotonNetwork.GameVersion = _gameVersion;

            var authValues = new AuthenticationValues {AuthType = CustomAuthenticationType.Custom};
            authValues.AddAuthParameter("username", _playFabManager.UserID);
            authValues.AddAuthParameter("token", _playFabManager.PhotonToken);

            PhotonNetwork.AuthValues = authValues;

            if (PhotonNetwork.IsConnected) return;
            PhotonNetwork.ConnectUsingSettings();
        }

        /// <summary>
        ///     Registers all our custom types
        /// </summary>
        private bool RegisterCustomTypes()
        {
            return PhotonPeer.RegisterType(typeof(ChessMove), byte.MaxValue, ChessMove.Serialize,
                ChessMove.Deserialize);
        }

        /// <summary>
        ///     Attempts to create a new room on the Photon network.
        /// </summary>
        /// <param name="passCodeRestricted">Whether the room can only be joined via secret code.</param>
        /// <param name="gameMode">The gamemode of the lobby to create.</param>
        /// <returns>Whether the room was successfully created and joined.</returns>
        public bool CreateRoom(bool passCodeRestricted = false, GameModeEnum gameMode = GameModeEnum.Chess)
        {
            var lobbyProperties = new Hashtable {{"GM", GameModeEnum.Chess}};
            var exposedProperties = new[] {"GM"};
            var options = new RoomOptions
            {
                MaxPlayers = 2, IsVisible = !passCodeRestricted, CustomRoomProperties = lobbyProperties,
                CustomRoomPropertiesForLobby = exposedProperties
            };
            return PhotonNetwork.CreateRoom(RandomString(5).ToUpperInvariant(), options);
        }


        /// <summary>
        ///     Joins a random available room.
        /// </summary>
        /// <returns>Whether the room was successfully joined.</returns>
        public bool JoinRandomRoom()
        {
            return PhotonNetwork.JoinRandomRoom();
        }

        /// <summary>
        ///     Join a room by code.
        /// </summary>
        /// <param name="code">The code belonging to the room to join.</param>
        /// <returns>Whether the room was successfully joined.</returns>
        public bool JoinRoomByCode(string code)
        {
            return PhotonNetwork.JoinRoom(code.ToUpperInvariant());
        }


        /// <summary>
        /// Generates a random string of hard-to-confuse characters.
        /// </summary>
        /// <param name="length">The number of characters to return in the string.</param>
        /// <returns></returns>
        private static string RandomString(int length)
        {
            var random = new Random();
            const string chars = "ABCDEFGHJKMNPQRTUVWXY3456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        //We clean up our connection to the event.
        public void Dispose()
        {
            _playFabManager.AuthenticatedEvent -= Connect;
        }
    }
}