using ExitGames.Client.Photon;
using olympia.Networking;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.Events;

namespace olympia
{
    public class GameUnityEvent : MonoBehaviourPunCallbacks, IOnEventCallback
    {
        [SerializeField] private UnityEvent _onMatchStart;

        public void OnEvent(EventData photonEvent)
        {
            if (photonEvent.Code == EventCodes.StartGameEventCode)
            {
                Debug.Log("Starting match...");
                _onMatchStart.Invoke();
            }
        }
    }
}